# Copyright 1998-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

import calendar
import datetime
import dateutil.tz

from django.shortcuts import get_object_or_404
from www.models import SiteSettings, Menys, SubMenys

def default_siteinfo(request, menyrequest):
    siteinfo = {}
    siteinfo['site'] = get_object_or_404(SiteSettings)
    activemeny = get_object_or_404(Menys, name = menyrequest)
    menys = Menys.objects.all().order_by('sort')
    #contact = get_object_or_404(SubPages, nav2 = 'contact')
    for meny in menys:
        if meny.title == 'Login' and request.user.is_authenticated:
            meny.show = False
        if meny.title == 'User' and request.user.is_authenticated:
            meny.show = True
        if meny.arg == '':
            meny.arg = False
    siteinfo['activemeny'] = activemeny
    siteinfo['menys'] = menys
    #siteinfo['contact'] = contact
    if activemeny.sub:
        submenys = SubMenys.objects.filter(MenyId = activemeny.id).order_by('sort')
        for submeny in submenys:
            if submeny.arg == '':
                submeny.arg = False
        siteinfo['submenys'] = submenys
        siteinfo['subactivemeny'] = False
    else:
        siteinfo['submenys'] = []
        siteinfo['subactivemeny'] = False
    return siteinfo

# time-handling methods

# this used to be a custom class; now it's just an instance of dateutil's class
UTC = dateutil.tz.tzutc()

def epoch2datetime(epoch):
    """Convert a UNIX epoch time to a datetime object, in the UTC timezone"""
    if epoch is not None:
        return datetime.datetime.fromtimestamp(epoch, tz=UTC)
    return None


def datetime2epoch(dt):
    """Convert a non-naive datetime object to a UNIX epoch timestamp"""
    if dt is not None:
        return calendar.timegm(dt.utctimetuple())
    return None
