from django.contrib import admin

from .models import SiteSettings, Menys, SubMenys

admin.site.register(SiteSettings)
admin.site.register(Menys)
admin.site.register(SubMenys)
