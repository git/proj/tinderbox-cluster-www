# Copyright 1998-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

class WWWRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on zobcs models to 'zobcs'"
        if model._meta.app_label == 'www':
            return 'default'
        return 'gentoo-ci'

    def db_for_write(self, model, **hints):
        "Point all operations on zobcs models to 'zobcs'"
        if model._meta.app_label == 'www':
            return 'default'
        return 'gentoo-ci'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in zobcs app"
        if obj1._meta.app_label == 'www' and obj2._meta.app_label == 'www':
            return True
        # Allow if neither is zobcs app
        elif 'www' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == 'gentoo-ci':
            return app_label == 'gentoo-ci'
        elif app_label == 'gentoo-ci':
            return False
        return True
