# Copyright 1998-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

from django.db import models

class SiteSettings(models.Model):
    id = models.IntegerField(primary_key=True)
    site = models.CharField(max_length=20)
    title = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    description = models.CharField(max_length=100)
    url = models.CharField(max_length=50)
    contact = models.CharField(max_length=50)
    class Meta:
        db_table = 'site_settings'
    def __str__(self):
        return '%s %s %s %s %s %s %s' % (self.id, self.site, self.title, self.email, self.description, self.url, self.contact)

class Menys(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
    view = models.BooleanField(default=False)
    sort = models.IntegerField(default=0)
    url = models.CharField(max_length=200)
    arg = models.CharField(max_length=50, blank=True)
    access = models.BooleanField(default=False)
    sub = models.BooleanField(default=False)
    class Meta:
        db_table = 'menys'
    def __str__(self):
        return  '%s %s %s %s %s %s %s %s %s %s' % (self.id, self.title, self.description, self.name, self.view, self.sort, self.url, self.arg, self.access, self.sub)

class SubMenys(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    MenyId = models.ForeignKey(Menys, on_delete=models.CASCADE, db_column='meny_id')
    name = models.CharField(max_length=100)
    view = models.BooleanField(default=False)
    sort = models.IntegerField(default=0)
    url = models.CharField(max_length=200)
    arg = models.CharField(max_length=50, blank=True)
    access = models.BooleanField(default=False)
    class Meta:
        db_table = 'sub_menys'
    def __str__(self):
        return  '%s %s %s %s %s %s %s %s %s %s' % (self.id, self.title, self.description, self.MenyId, self.name, self.view, self.sort, self.url, self.arg, self.access)

class Posts(models.Model):
    id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table='posts'
    def __str__(self):
        return '%s %s %s %s %s' % (self.id, self.title, self.url, self.text, self.created_at)

class Sponsors(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    img = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    alt = models.CharField(max_length=200)
    weight =  models.IntegerField(default=0)
    active = models.BooleanField(default=False)
    class Meta:
        db_table = 'sponsors'
    def __str__(self):
        return '%s %s %s %s %s %s %s' % (self.id, self.name, self.img, self.link, self.alt, self.weight, self.active)
