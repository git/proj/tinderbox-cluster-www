# Copyright 1998-2019 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

from django.http import HttpResponse
from django.shortcuts import render
from django.conf import settings

from .utils import default_siteinfo
from .models import Posts

def index(request):
    menyrequest = 'www'
    siteinfo = default_siteinfo(request, menyrequest)
    siteinfo['posts'] = Posts.objects.all()[:2]
    htmlrequest = menyrequest + '/index.html'
    return render(request, htmlrequest, siteinfo)
