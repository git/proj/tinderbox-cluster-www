from django.db import models

class Categories(models.Model):
	CategoryId = models.IntegerField(primary_key=True, db_column='category_id')
	Category = models.CharField(max_length=150, db_column='category')
	Active = models.BooleanField(db_column='active')
	TimeStamp = models.DateTimeField(db_column='time_stamp')
	class Meta:
		db_table = 'categories'
	def __str__(self):
		return '%s %s %s %s' % (self.CategoryId, self.Category, self.Active, self.TimeStamp)

class Repos(models.Model):
	RepoId = models.IntegerField(primary_key=True, db_column='repo_id')
	Repo = models.CharField(max_length=100, db_column='repo')
	class Meta:
		db_table = 'repos'
	def __str__(self):
		return '%s %s' % (self.RepoId, self.Repo)

class Packages(models.Model):
	PackageId = models.IntegerField(primary_key=True, db_column='package_id')
	CategoryId = models.ForeignKey(Categories, db_column='category_id')
	Package = models.CharField(max_length=150, db_column='package')
	RepoId = models.ForeignKey(Repos, db_column='repo_id')
	Checksum = models.CharField(max_length=100, db_column='checksum')
	Active = models.BooleanField(db_column='active')
	TimeStamp = models.DateTimeField(db_column='time_stamp')
	class Meta:
		db_table = 'packages'
	def __str__(self):
		return '%s %s %s %s %s %s %s' % (self.PackageId, self.CategoryId, self.Package, self.RepoId, self.Checksum, self.Active, self.TimeStamp)

class Ebuilds(models.Model):
	EbuildId = models.IntegerField(primary_key=True, db_column='ebuild_id')
	PackageId = models.ForeignKey(Packages, db_column='package_id')
	Version = models.CharField(max_length=150, db_column='version')
	Checksum = models.CharField(max_length=100, db_column='checksum')
	Active = models.BooleanField(db_column='active')
	TimeStamp = models.DateTimeField(db_column='time_stamp')
	class Meta:
		db_table = 'ebuilds'
	def __str__(self):
		return '%s %s %s %s %s %s' % (self.EbuildId, self.PackageId, self.Version, self.Checksum, self.Active, self.TimeStamp)

