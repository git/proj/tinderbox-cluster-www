from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('www/', include('www.urls')),
    #path('auth/', include('gosbs_auth.urls')),
    #path('projects/', include('projects.urls', namespace="projects")),
    #path('admin/', admin.site.urls),
]
